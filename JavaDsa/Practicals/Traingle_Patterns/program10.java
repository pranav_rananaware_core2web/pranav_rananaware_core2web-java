import java.io.*;
class Pattern10{

public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows : ");
		int rows = Integer.parseInt(br.readLine());
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=rows-i+1;j++){
				if(i%2==0)
					if(j%2==0)
						System.out.print((64+j+i-1)+" ");
					else
						System.out.print((char)(64+j+i-1)+" ");
				else
					if(j%2==0)
						System.out.print((char)(64+j+i-1)+" ");
					else
						System.out.print((64+j+i-1)+" ");

			}
			System.out.println();
		}
	}
}
